# **Procesamiento de tuits buscando tendencias.**

##   **1. Captura de tuits mediante streaming.**
En esta primera parte usaremos la base de streaming con spark dada en el laboratorio https://gitlab.pervasive.it.uc3m.es/distributed-computing-assignements/3-bigdata-spark-streaming#repositorio, gracias a Daniel Diaz.

Como hemos visto, crearemos un proyecto llamado **TwitterStreaming** y añadiremos estas dependencias después de haber convertido el proyecto a Maven.

```xml
	<dependencies>
		<dependency>
			<groupId>org.twitter4j</groupId>
			<artifactId>twitter4j-stream</artifactId>
			<version>[4.0,)</version>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>3.8.1</version>
		</dependency>
	</dependencies>
```

Acto seguido, debemos meter esta clase al proyecto (al package cdist, para ser exactos), en el que meteremos nuestros **tokens** de desarrollador (_consumerKey, _consumerSecret,  _accessToken y _accessTokenSecret) dados por Twitter en https://developer.twitter.com/en.

Más adelante también añadiremos un escáner de las palabras interesadas y un contador **numeroTwits** para simplemente contar el numero de tuits que capturamos con las palabras que nos interesan.

```java
package cdist;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.nio.channels.ServerSocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterProducer {

	public static final String _consumerKey = "XXXXXXXXXXXXXXXXXXXXXXXXX";
	public static final String _consumerSecret = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
	public static final String _accessToken = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
	public static final String _accessTokenSecret = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    private static final boolean EXTENDED_TWITTER_MODE = true;

	static int numeroTwits = 0;

	public class Handler  {

		public AsynchronousSocketChannel client;
		public ByteBuffer out = ByteBuffer.allocate(1024);
		public ByteBuffer in = ByteBuffer.allocate(1024);

	}

	public static void main(String[] args) {
		TwitterProducer tp = new TwitterProducer();

		ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
		configurationBuilder.setOAuthConsumerKey(_consumerKey).setOAuthConsumerSecret(_consumerSecret)
				.setOAuthAccessToken(_accessToken).setOAuthAccessTokenSecret(_accessTokenSecret);

		AsynchronousServerSocketChannel server;
		AsynchronousSocketChannel client = null;
		Handler h = tp.new Handler();

		try {
			server = AsynchronousServerSocketChannel.open();
			server.bind(new InetSocketAddress("127.0.0.1", 9999));
			server.accept(h, new CompletionHandler<AsynchronousSocketChannel, Handler>() {

				@Override
				public void completed(AsynchronousSocketChannel result, Handler handler) {
					// prepare for future connections
					if (server.isOpen())
						server.accept(null, this);

					if (result != null && result.isOpen()) {
						handler.client = result;

					}

				}

				@Override
				public void failed(Throwable exc, Handler attachment) {
					// TODO Auto-generated method stub

				}
			});

		} catch (IOException e) {

		}

		TwitterStream twitterStream = new TwitterStreamFactory(configurationBuilder.build()).getInstance();

		twitterStream.addListener(new StatusListener() {

			/* when a new tweet arrives */
			public void onStatus(Status status) {
				
				numeroTwits++;

				System.out.println(status.getText());
				if (h != null) {
					if (h.client != null) {
						String tweet = status.getText();
						h.out.clear();
						h.out.put(tweet.getBytes(StandardCharsets.UTF_8));
						h.out.flip();
						h.client.write(h.out);
					}
				}
			}

			@Override
			public void onException(Exception arg0) {
				System.out.println("Exception on twitter");

			}

			@Override
			public void onDeletionNotice(StatusDeletionNotice arg0) {
				System.out.println("Exception on twitter");

			}

			@Override
			public void onScrubGeo(long arg0, long arg1) {
				System.out.println("onScrubGeo");

			}

			@Override
			public void onStallWarning(StallWarning arg0) {
				System.out.println("EonStallWarning");

			}

			@Override
			public void onTrackLimitationNotice(int arg0) {
				System.out.println("EonTrackLimitationNotice");

			}
		});

		FilterQuery tweetFilterQuery = new FilterQuery(); // See
		tweetFilterQuery.track(new String[] { "spain" }); // , "Teletubbies"}); // OR on keywords

		// ejemplo de localización  (desde USA)
		// tweetFilterQuery.locations(new double[][]{new  double[]{-126.562500,30.448674}, new double[]{-61.171875,44.087585 }});
		// See https://dev.twitter.com/docs/streaming-apis/parameters#locations for
		// proper location doc.
		// Note that not all tweets have location metadata set.
		// ejemplo de idioma  (en inglés)
		/* tweetFilterQuery.language(new String[]{"en"}); */ 
		twitterStream.filter(tweetFilterQuery);
	}
}
```

En la práctica de Computación Distribuida se usó el ejemplo de spain, en el que simplemente se mostraban todos los tuits que contienen esa palabra , desde el momento en el que se ejecuta, directamente desde Twitter.

* `¿**Que palabras**? Para nuestro proyecto, deberemos usar diferentes palabras.`



### - **Preprocesamiento de tuits.**
Ahora **"limpiaremos"** los tuits para facilitar el posterior procesamiento, aunque no guardaremos ni nos interesa en este caso el contenido de los tuits.

Como hemos visto, también en la práctica, en cada proyecto que queramos limpiar los retornos de carro o emoticonos, deberemos
introducir estas dependencias.

```xml
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>3.8.1</version>
		</dependency>
		<dependency>
			<groupId>com.vdurmont</groupId>
			<artifactId>emoji-java</artifactId>
			<version>4.0.0</version>
		</dependency>
```
Para después usar este código allá donde capturemos los tuits.

```java
// text almacena el tweet original
String text = status.getText();
// elminamos los retornos de carro
String noBreaksTweet = text.replace("\n", "").replace("\r", "");
// procesado de tweets con emojis (dos posibilidades)
// 1) eliminar los emojis (resultado, tweet sin emojis ni retornos de carro)
noBreaksNoEmojisTweet = EmojiParser.removeAllEmojis(noBreaksTweet);

```

A partir de aquí tenemos la posibilidad de usar Spark para capturar los tuits con la clase vista en una de las prácticas y ejecutando esto en el terminal, aunque no es necesario para lo que estábamos haciendo en este momento.

<img src= "https://gitlab.com/jabipaco/proyecto-computacion-distribuida/-/raw/7e27faacf850ee3be72fa3735b6bd187590f8675/images/Properties_ejecucion.png" width="400px">

##  **2. Procesamiento de tuits.**

Ahora añadiremos un **contador** y un **archivo** al inicio de la clase, para guardar el numero de tweets de cada búsqueda en un archivo aparte.

```java
static int contador=0;
//IMPORTANTE: Cambiar adecuadamente la ruta.
static File fichero = new File ("/var/[RUTA PROPIA]/data.txt");
```

A continuación, en el método **onStatus** a cargo de capturar cada tweet, introduciremos este código y lo procesaremos escribiendo el contador cada vez que este aumente.

```java
	contador++;
	// sobreescribir palabra + contador	
	FileWriter fw;
	try {
		fw = new FileWriter(fichero, true);
				
		BufferedWriter bw = new BufferedWriter(fw);
				
		if( contador==1) {
			bw.write(" "+ String.valueOf(contador));
		}else{bw.write(" " + String.valueOf(contador));  
		}
				
		bw.close();
		fw.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
```

También añadiremos un **escáner de palabras** en la consola para capturar tweets con la palabra que te interese, aparte del análisis que vamos a hacer.

Por último, al final de esta clase añadiremos la palabra buscada para luego facilitar la creación de la gráfica y para el mejor entendimiento del fichero y del programa.

```java
Scanner scanner = new Scanner(System.in);
		System.out.println("Ingrese palabra");
		String palabra = scanner.nextLine();
		
		FileWriter fw;
		try {
			fw = new FileWriter(fichero, true);
		
		BufferedWriter bw = new BufferedWriter(fw);
		
		bw.write("\n"+palabra);
		bw.close();
		fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
```

### - **Creación de gráficas con java.**
En primer lugar crearemos una nueva clase llamada **grafica.java** donde usaremos FreeChart y para ello necesitaremos descargar la librería .jar y añadirla al proyecto.

<img src="https://gitlab.com/jabipaco/proyecto-computacion-distribuida/raw/master/images/FreeChartJAR.jpeg" width="400px">

Ahora sí obtendremos las gráficas mediante este código de Java.

``` java

public class gráfica {
	
	// IMPORTANTE: Cambiar adecuadamente la ruta
    static File fichero = new File ("/var/[RUTA PROPIA]/data.txt");
	
	public static void main(String[] args) throws IOException {
		
		FileReader fr = new FileReader (fichero);
		BufferedReader br = new BufferedReader(fr);
		
		// Lectura del fichero
        String linea;
        String palabra;
        int numPalabras;
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        	
        while((linea=br.readLine())!=null) {
           
           		String[] output = linea.split(" "); 
           		
           		int nT=Integer.parseInt(output[output.length-1]);
           
           		dataset.setValue( nT, "Palabras", output[0]);
        
        }
		br.close();
		fr.close();
		
		
		JFreeChart chart = ChartFactory.createBarChart3D("Tweets", "palabra",
				"Número Tweets", dataset, PlotOrientation.VERTICAL, true,
				true, false);
	
		
		// Creación del panel con el gráfico
		ChartPanel panel = new ChartPanel(chart);

		JFrame ventana = new JFrame("El gráfico");
		ventana.getContentPane().add(panel);
		ventana.pack();
		ventana.setVisible(true);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//ChartUtilities.saveChartAsJPEG(new File("grafico.jpg"), chart, 500, 300);
	
	}
}

```

Después de introducir esta clase, se podrá cambiar los letreros, el nombre de la gráfica, del archivo y de cualquier cosa que se deseara.

Por ejemplo, añadiremos este código al final de la clase para tener la posibilidad de borrar el archivo con las palabras capturadas y el número de tweets, para tener la posibilidad de hacer otra búsqueda independiente después.

``` java

Scanner scanner = new Scanner(System.in);

System.out.println("¿Desea limpiar el archivo? (y/n)");
String limpiar = scanner.nextLine();

if(limpiar.equals("y")==true) {
    FileWriter fw = new FileWriter(fichero);
    BufferedWriter bw = new BufferedWriter(fw);
    bw.write("");
    bw.close();
    fw.close();
    System.out.println("Archivo limpio");
}else {
    System.out.println("Manteniendo archivo");
}

```


##  **3. Análisis del procesamiento con imágenes.**
En esta última parte veremos como analizamos cada gráfico y la posible utilidad real que tienen.

Hemos pensado en darle un enfoque sobre la actualidad (sobretodo de jóvenes) en Internet, buscando tendencias sobre redes sociales, juegos online, dónde verlos.

Haremos una comparación de todos ellos durante un minuto viendo la relevancia y las tendencias que tienen.

Aquí tenemos un ejemplo comparando WhatsApp y Telegram, dos de las mayores aplicaciones de mensajería instantánea.

<img src= "https://gitlab.com/jabipaco/proyecto-computacion-distribuida/raw/master/images/grafica1.png" width="400px">

Ahora, analizaremos las diferencias entre Twitch y YouTUbe, dos aplicaciones muy usadas por los jóvenes para ver vídeos y streams sobre videojuegos, aunque entendemos la diferencia también a que YouTube también es muy usada en cuanto a música.

<img src= "https://gitlab.com/jabipaco/proyecto-computacion-distribuida/raw/master/images/grafica2.png" width="400px">

Siguiendo esta línea, compararemos los videojuegos más jugados y más vistos, por ejemplo, en Twitch.

<img src= "https://gitlab.com/jabipaco/proyecto-computacion-distribuida/raw/master/images/grafica3.png" width="400px">

Por último, compararemos posiblemente las 4 grandes redes sociales mundiales, Instagram, Facebook, Twitter y Snapchat...

<img src= "https://gitlab.com/jabipaco/proyecto-computacion-distribuida/raw/master/images/grafica4.png" width="400px">


### - **Conclusión.**
